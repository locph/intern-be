const path = require('path');

const { v4 } = require('uuid');
const multer = require('multer');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, path.join(path.dirname(require.main.filename), 'upload'));
  },
  filename: (req, file, cb) => {
    cb(null, v4() + file.originalname);
  },
});

const upload = multer({ storage: storage });

module.exports = upload;
