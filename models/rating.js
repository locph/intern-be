const mongoose = require('mongoose');

const ratingSchema = new mongoose.Schema(
	{
		userId: {
			type: mongoose.Schema.Types.ObjectId,
			required: true,
			ref: 'user',
		},
		productId: {
			type: mongoose.Schema.Types.ObjectId,
			required: true,
			ref: 'product',
		},
		title: {
			type: String,
			trim: true,
		},
		comment: {
			type: String,
			trim: true,
		},
		rating: {
			type: Number,
			required: true,
			min: 1,
			max: 5,
		},
	},
	{ timestamps: true },
);

module.exports = mongoose.model('rating', ratingSchema);
