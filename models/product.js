const mongoose = require('mongoose');

const productSchema = new mongoose.Schema(
	{
		name: {
			type: String,
			required: true,
		},
		categories: [{ type: String }],
		sold: { type: Number, default: 0 },
		brand: String,
		price: Number,
		size: [{ type: String }],
		color: [{ type: String }],
		quantity: Number,
		description: String,
		photos: [{ type: String }],
		rating: {
			totalRating: {
				type: Number,
				default: 0,
			},
			totalScore: {
				type: Number,
				default: 0,
			},
		},
	},
	{ timestamps: true },
);

module.exports = mongoose.model('product', productSchema);
