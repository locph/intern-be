const router = require('express').Router();
const { body } = require('express-validator');

const AuthController = require('../controllers/authentication.controller');

const AuthMiddleware = require('../middlewares/authenication.middleware');

// /auth/sign-up
router.post(
  '/sign-up',
  [
    body('email', 'Invalid e-mail!').normalizeEmail().isEmail(),
    body('name', 'Invalid name!').notEmpty(),
    body('password', 'Password must be more than 6 characters!').isLength({
      min: 6,
    }),
  ],
  AuthController.postCustomerSignUp
);

// /auth/log-in
router.post('/log-in', AuthController.postCustomnerLogIn);

// /auth/auto-log-in
router.post('/auto-log-in', AuthController.postCustomerAutoLogin);

module.exports = router;
