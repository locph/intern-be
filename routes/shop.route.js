const router = require('express').Router();

const ShopController = require('../controllers/shop.controller');

// /shop/products
router.get('/products', ShopController.getAllProducts);

// /shop/products/search
router.get('/products/search', ShopController.getSearchProductByName);

// /shop/product
router.get('/product/:productId', ShopController.getProductById);

module.exports = router;
