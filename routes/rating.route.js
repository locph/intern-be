const router = require('express').Router();
const { body } = require('express-validator');

const RatingController = require('../controllers/rating.controller');

const AuthMiddleware = require('../middlewares/authenication.middleware');

// /rating/
router.get('/:productId', RatingController.getRatingByProductId);

router.post(
  '/:productId',
  AuthMiddleware.isCustomerAuthenticate,
  [body('rating', 'Invalid rating!').isInt()],
  RatingController.postNewRating
);

router.delete(
  '/:productId/:ratingId',
  AuthMiddleware.isCustomerAuthenticate,
  RatingController.deleteRatingById
);

module.exports = router;
