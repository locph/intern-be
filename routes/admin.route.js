const router = require('express').Router();

const multer = require('../models/multer');

const AuthenticationController = require('../controllers/authentication.controller');
const AdminController = require('../controllers/admin.controller');

const AuthenticationMiddleware = require('../middlewares/authenication.middleware');

// /admin/log-in
router.post('/log-in', AuthenticationController.postAdminLogin);

// all api bellow should attach token in header
router.use(AuthenticationMiddleware.isAdminAuthenticate);

// /admin/product
router.get('/product/:productId', AdminController.getProductById);
router.put('/product/:productId', AdminController.putProductById);
router.delete('/product/:productId', AdminController.deleteProductById);
router.post('/product', multer.any(), AdminController.postProduct); 

// /admin/product
router.get('/products', AdminController.getProducts);

module.exports = router;
