const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const mongoose = require('mongoose');
const { ApolloServer } = require('apollo-server-express');
const { createServer } = require('http');
require('dotenv').config();

const HttpError = require('./models/http-error');

const AdminRoutes = require('./routes/admin.route');
const AuthRoutes = require('./routes/auth.route');
const ShopRoutes = require('./routes/shop.route');
const RatingRoutes = require('./routes/rating.route');

const typeDefs = require('./graphql/schema');
const resolvers = require('./graphql/resolvers');

const { authGraphql } = require('./middlewares/auth.graphql');

const MONGOURI = process.env.MONGODB_URI;
const { PORT = 4808 } = process.env;

async function startApolloServer() {
  const app = express();
  const server = new ApolloServer({
    typeDefs,
    resolvers,
    subscriptions: {
      onConnect: () => {
        console.log(`New client connected`);
      },
      onDisconnect: () => {
        console.log(`Client disconnected`);
      },
    },
    context: async ({ req }) => {
      const { userId, token } = await authGraphql(req);
      return { userId, token };
    },
  });

  server.applyMiddleware({ app });

  app.use(cors());
  app.use(morgan('dev'));
  app.use(express.json());
  app.use(express.urlencoded({ extended: false }));

  app.use('/images', express.static('upload'));

  app.use('/admin', AdminRoutes);
  app.use('/auth', AuthRoutes);
  app.use('/shop', ShopRoutes);
  app.use('/rating', RatingRoutes);

  // TODO: 404 error
  app.use((req, res, next) => {
    return next(new HttpError('404 not found!', 404));
  });

  // TODO: Error Handling
  app.use((err, req, res, next) => {
    const { message = 'Error!', status = 500 } = err;
    res.status(status).json({ message });
  });

  const httpServer = createServer(app);
  server.installSubscriptionHandlers(httpServer);

  await mongoose.connect(MONGOURI, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
  });

  await httpServer.listen(PORT);
  console.log(
    `🚀 Server ready at http://localhost:${PORT}${server.graphqlPath}`
  );

  console.log(
    `🚀 Subscriptions ready at ws://localhost:${PORT}${server.subscriptionsPath}`
  );

  return { server, app };
}

startApolloServer();

// module.exports = app;
