const { gql } = require('apollo-server-express');

module.exports = gql`
  type Product {
    _id: String
    name: String
    price: Int
    quantity: Int
    color: String
    size: String
    photo: String
    key: String
    userId: String
  }

  input InputProduct {
    _id: ID!
    color: String!
    size: String!
    quantity: Int!
  }

  type SubProduct {
    product: Product
    type: String!
  }

  type Query {
    hello: String
  }

  type Mutation {
    addToCart(_id: ID, color: String, quantity: Int, size: String): Product!
    removefromCart(_id: ID, size: String, color: String): Product
    changeQuantity(_id: ID, size: String, color: String, quantity: Int): Product
    checkout(products: [InputProduct]): String
    getNewTokenByRefreshToken(refreshToken: String): String
    logoutDevice(refreshToken: String): String
    logoutAllDevices: String
  }

  type Subscription {
    listenAddToCart(userId: String): SubProduct
  }

  schema {
    query: Query
    mutation: Mutation
    subscription: Subscription
  }
`;
