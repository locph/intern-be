require('dotenv').config();
const { PubSub, withFilter } = require('apollo-server-express');
const jwt = require('jsonwebtoken');

const Product = require('../models/product');
const Order = require('../models/order');

const { getClient } = require('../redis');

const pubsub = new PubSub();

const CLEAR_CART = 'CLEAR_CART';
const ADD_PRODUCT = 'ADD_PRODUCT';
const REMOVE_PRODUCT = 'REMOVE_PRODUCT';
const CHANGE_QUANTITY_PRODUCT = 'CHANGE_QUANTITY_PRODUCT';
const FORCE_LOGOUT = 'FORCE_LOGOUT';

module.exports = {
  Query: {
    hello: () => {
      return '';
    },
  },

  Mutation: {
    addToCart: async (_, receivedProduct, { userId }) => {
      if (!userId) {
        const error = new Error('Not authenticate!');
        error.code = 401;
        throw error;
      }

      const { _id, color, quantity, size } = receivedProduct;
      const product = await Product.findById(_id);

      const asyncProduct = {
        ...product._doc,
        color,
        quantity,
        size,
        photo: product.photos[0],
        key: _id + color + size,
      };

      pubsub.publish(ADD_PRODUCT, {
        listenAddToCart: {
          product: asyncProduct,
          type: ADD_PRODUCT,
          userId,
        },
      });

      return asyncProduct;
    },

    removefromCart: (_, removeProduct, { userId }) => {
      if (!userId) {
        const error = new Error('Not authenticate!');
        error.code = 401;
        throw error;
      }

      pubsub.publish(REMOVE_PRODUCT, {
        listenAddToCart: {
          product: removeProduct,
          type: REMOVE_PRODUCT,
          userId,
        },
      });

      return removeProduct;
    },

    changeQuantity: (_, product, { userId }) => {
      if (!userId) {
        const error = new Error('Not authenticate!');
        error.code = 401;
        throw error;
      }

      pubsub.publish(CHANGE_QUANTITY_PRODUCT, {
        listenAddToCart: {
          product,
          type: CHANGE_QUANTITY_PRODUCT,
          userId,
        },
      });
      return product;
    },

    checkout: async (_, { products }, { userId }) => {
      if (!userId) {
        const error = new Error('Not authenticate!');
        error.code = 401;
        throw error;
      }

      for (const p of products) {
        const product = await Product.findById(p._id);
        p.price = product.price;
        p.productId = p._id;
        delete p._id;
      }

      const order = new Order({ products, userId });

      await order.save();

      pubsub.publish(CLEAR_CART, {
        listenAddToCart: {
          type: CLEAR_CART,
          userId,
        },
      });

      return order._id;
    },

    getNewTokenByRefreshToken: async (_, { refreshToken }) => {
      const decoded = jwt.decode(
        refreshToken,
        process.env.USER_REFRESH_JWT_SECRET_KEY
      );
      const now = ~~(Date.now() / 1000);

      if (!decoded || decoded.exp < now) {
        const error = new Error('Invalid refresh token!');
        error.code = 403;
        throw error;
      }

      const { _id, name, email } = decoded;

      const client = await getClient();
      const refreshTokens = await client.lrange(`refresh_token_${_id}`, 0, -1);

      if (!refreshTokens.includes(refreshToken)) {
        const error = new Error('Invalid refresh token!');
        error.code = 403;
        throw error;
      }

      const token = jwt.sign(
        { _id, name, email },
        process.env.USER_JWT_SECRET_KEY,
        {
          expiresIn: '10 seconds',
        }
      );

      await client.rpush(`access_token_${_id}`, token);
      await client.quit();

      return token;
    },

    logoutDevice: async (_, { refreshToken }, { userId, token }) => {
      if (!userId) {
        const error = new Error('Not authenticate!');
        error.code = 401;
        throw error;
      }

      const client = await getClient();
      const accessTokens = await client.lrange(`access_token_${userId}`, 0, -1);
      const refreshTokens = await client.lrange(
        `refresh_token_${userId}`,
        0,
        -1
      );

      await client.del(`access_token_${userId}`);
      const newAccessTokens = accessTokens.filter((at) => at !== token);
      if (newAccessTokens.length) {
        for (const t of newAccessTokens) {
          await client.rpush(`access_token_${userId}`, t);
        }
      }

      await client.del(`refresh_token_${userId}`);
      const newRefreshTokens = refreshTokens.filter(
        (rt) => rt !== refreshToken
      );
      if (newRefreshTokens.length) {
        for (const rt of newRefreshTokens) {
          await client.rpush(`refresh_token_${userId}`, rt);
        }
      }

      await client.quit();
      return 'Clear redis success!';
    },

    logoutAllDevices: async (_, {}, { userId }) => {
      if (!userId) {
        const error = new Error('Not authenticate!');
        error.code = 401;
        throw error;
      }

      const client = await getClient();
      await client.del(`refresh_token_${userId}`);
      await client.del(`access_token_${userId}`);
      await client.quit();

      pubsub.publish(FORCE_LOGOUT, {
        listenAddToCart: {
          type: FORCE_LOGOUT,
          userId,
        },
      });

      return 'Clear redis success!';
    },
  },

  Subscription: {
    listenAddToCart: {
      subscribe: withFilter(
        () =>
          pubsub.asyncIterator([
            ADD_PRODUCT,
            REMOVE_PRODUCT,
            CHANGE_QUANTITY_PRODUCT,
            CLEAR_CART,
            FORCE_LOGOUT,
          ]),
        (payload, variables) => {
          return payload.listenAddToCart.userId === variables.userId;
        }
      ),
    },
  },
};
