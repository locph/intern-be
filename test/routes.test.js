const request = require('supertest');
const mongoose = require('mongoose');
require('dotenv').config();

// const app = require('../server');

const AuthServices = require('../services/authentication.service');

describe('POST /admin/log-in', () => {
  it('it should login fail', async () => {
    const token = await AuthServices.adminLogin('alwkjef', '123456');

    expect(token).toThrowError(/Wrong/);
    // const res = await request(app).post('/admin/log-in').send({
    //   email: 'alfjalwef',
    //   password: 'abcdef',
    // });

    // expect(res.statusCode).toBe(401);
    // expect(res.body).toHaveProperty('message');
  });

  it('it should login success', async () => {
    const response = await AuthServices.adminLogin('admin@admin.com', '123456');

    expect(response).toHaveProperty('token');
    expect(response).toHaveProperty('_id');
    expect(response).toHaveProperty('name');
  });
});

beforeAll(async (done) => {
  try {
    await mongoose.connect(process.env.MONGODB_URI, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
    });
    done();
  } catch (err) {
    console.log(err);
    done();
  }
});

afterAll(async (done) => {
  // Closing the DB connection allows Jest to exit successfully.
  try {
    await mongoose.connection.close();
    done();
  } catch (error) {
    console.log(error);
    done();
  }
  // done()
});
