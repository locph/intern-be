const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
require('dotenv').config();

const Admin = require('../models/admin');
const HttpError = require('../models/http-error');

exports.adminLogin = async (email, password) => {
  const admin = await Admin.findOne({ email });

  // wrong email / password
  if (!admin) {
    throw new HttpError('Wrong email/password!', 401);
  }

  const isPwMatch = bcrypt.compareSync(password, admin.password);

  if (!isPwMatch) {
    throw new HttpError('Wrong email/password!', 401);
  }

  const token = jwt.sign(
    {
      _id: admin._id,
      name: admin.name,
    },
    process.env.ADMIN_JWT_SECRET_KEY,
    { expiresIn: '3d' }
  );

  return { _id: admin._id, name: admin.name, token };
};
