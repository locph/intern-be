const redis = require('redis');
const asyncRedis = require('async-redis');

module.exports = {
  async getClient() {
    const client = await redis.createClient();

    return asyncRedis.decorate(client);
  },
};
