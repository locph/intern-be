const jwt = require('jsonwebtoken');
require('dotenv').config();

const HttpError = require('../models/http-error');

exports.isAdminAuthenticate = (req, res, next) => {
  try {
    const token = req.get('Authorization').split(' ')[1];

    const decodedToken = jwt.verify(token, process.env.ADMIN_JWT_SECRET_KEY);

    req._id = decodedToken._id;

    next();
  } catch (err) {
    return next(new HttpError('Invalid Auth Token!', 401));
  }
};

exports.isCustomerAuthenticate = (req, res, next) => {
  try {
    const token = req.get('Authorization').split(' ')[1];

    const decodedToken = jwt.verify(token, process.env.USER_JWT_SECRET_KEY);

    req.userId = decodedToken._id;

    next();
  } catch (err) {
    return next(new HttpError('Invalid Auth Token!', 401));
  }
};
