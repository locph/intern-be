const jwt = require('jsonwebtoken');
require('dotenv').config();

const { getClient } = require('../redis');
// access_token_[userId]
// refresh_token_[userId]

exports.authGraphql = async (req, res) => {
  let userId = null;
  let token = null;

  try {
    token = req.get('Authorization').split(' ')[1];
    const now = ~~(Date.now() / 1000);

    const decoded = jwt.decode(token, process.env.USER_JWT_SECRET_KEY);

    const client = await getClient();
    const tokens = await client.lrange(`access_token_${decoded._id}`, 0, -1);

    if (decoded.exp >= now) {
      await client.quit();
      if (tokens.includes(token)) {
        userId = decoded._id;
      }
    } else {
      if (tokens.includes(token)) {
        client.del(`access_token_${decoded._id}`);
        const newTokens = tokens.filter((t) => t !== token);

        for (const newToken of newTokens) {
          await client.rpush(`access_token_${decoded._id}`, newToken);
        }
      }

      await client.quit();
    }
  } catch (err) {}

  return { userId, token };
};
