require('dotenv').config();
const { validationResult } = require('express-validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const AuthenticationServices = require('../services/authentication.service');

const HttpError = require('../models/http-error');
const User = require('../models/user');

const { getClient } = require('../redis');

exports.postAdminLogin = async (req, res, next) => {
  // check email -> check password -> generate jwt
  const { email, password } = req.body;

  try {
    const response = await AuthenticationServices.adminLogin(email, password);

    res.json(response);
  } catch (err) {
    console.log(err);
    return next(err);
  }
};

exports.postCustomerSignUp = async (req, res, next) => {
  // TODO: validate form -> check user email -> create new user
  try {
    const validateResult = validationResult(req).array();

    if (validateResult.length) {
      return next(new HttpError(validationResult[0].msg, 422));
    }

    const { name, email, password } = req.body;

    const existUser = await User.findOne({ email });

    if (existUser) {
      return next(new HttpError('This email has been used!', 422));
    }

    const hashedPw = bcrypt.hashSync(password, 12);

    const user = new User({ name, email, password: hashedPw });

    await user.save();

    res.status(201).json({ message: `Sign up user ${user.email} successful!` });
  } catch (err) {
    console.log(err);
    return next(err);
  }
};

exports.postCustomnerLogIn = async (req, res, next) => {
  // TODO: find by email -> check pass -> generate jwt and sendback to client
  try {
    const { email, password } = req.body;

    const user = await User.findOne({ email });

    if (!user) {
      return next(new HttpError("This email doesn't exist!", 403));
    }

    const isMatchPw = bcrypt.compareSync(password, user.password);

    if (!isMatchPw) {
      return next(new HttpError('Wrong password!', 403));
    }

    const responseData = { _id: user._id, name: user.name, email: user.email };

    const token = jwt.sign(responseData, process.env.USER_JWT_SECRET_KEY, {
      expiresIn: '10 seconds',
    });

    const refreshToken = jwt.sign(
      responseData,
      process.env.USER_REFRESH_JWT_SECRET_KEY,
      { expiresIn: '7d' }
    );

    const client = await getClient();

    await client.rpush(`access_token_${user._id}`, token);
    await client.rpush(`refresh_token_${user._id}`, refreshToken);

    await client.quit();

    res.json({ ...responseData, token, refreshToken });
  } catch (err) {
    console.log(err);
    return next(err);
  }
};

exports.postCustomerAutoLogin = async (req, res, next) => {
  try {
    const { refreshToken } = req.body;

    const decoded = jwt.verify(
      refreshToken,
      process.env.USER_REFRESH_JWT_SECRET_KEY
    );

    const { _id, name, email } = decoded;

    const client = await getClient();
    const refreshTokens = await client.lrange(`refresh_token_${_id}`, 0, -1);
    if (!refreshTokens.includes(refreshToken)) {
      throw new Error();
    }

    const token = jwt.sign(
      { _id, name, email },
      process.env.USER_JWT_SECRET_KEY,
      {
        expiresIn: '10 seconds',
      }
    );

    await client.rpush(`access_token_${_id}`, token);

    await client.quit();

    res.json({ _id, name, email, token });
  } catch (err) {
    console.log(err);
    return next(new HttpError('Invalid refresh token!', 403));
  }
};
