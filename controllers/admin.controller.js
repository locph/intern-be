const Product = require('../models/product');

exports.postProduct = async (req, res, next) => {
  try {
    const { categories, color, size } = req.body;
    const product = new Product({
      ...req.body,
      categories: categories.split(','),
      color: color.split(','),
      size: size.split(','),
      photos: req.files.map((file) => file.filename),
    });

    const result = await product.save();

    res.json(result);
  } catch (err) {
    console.log(err);
    return next(err);
  }
};

exports.getProducts = async (req, res, next) => {
  try {
    const ITEM_PER_PAGE = 10;
    const { page = 1, sortBy = 'createAt', name = '' } = req.query;

    const findCondition = {
      name: {
        $regex: new RegExp(name, 'gi'),
      },
    };

    const totalProduct = await Product.find(findCondition).countDocuments();

    const products = await Product.find(findCondition)
      .sort({
        [sortBy]: 1,
      })
      .skip((page - 1) * ITEM_PER_PAGE)
      .limit(ITEM_PER_PAGE);

    res.json({ products, totalProduct });
  } catch (err) {
    console.log(err);
    return next(err);
  }
};

exports.getProductById = async (req, res, next) => {
  const { productId } = req.params;

  try {
    const product = await Product.findById(productId);

    res.json(product);
  } catch (err) {
    console.log(err);
    return next(err);
  }
};

exports.putProductById = async (req, res, next) => {
  try {
    const { productId } = req.params;

    const result = await Product.findByIdAndUpdate(productId, {
      $set: req.body,
    });

    res.json(result);
  } catch (err) {
    console.log(err);
  }
};

exports.deleteProductById = async (req, res, next) => {
  try {
    const { productId } = req.params;

    await Product.findByIdAndDelete(productId);

    res.end();
  } catch (err) {
    console.log(err);
    return next(err);
  }
};
