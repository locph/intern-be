const Product = require('../models/product');
const HttpError = require('../models/http-error');

exports.getAllProducts = async (req, res, next) => {
	try {
		const ITEM_PER_PAGE = 12;
		const {
			page = 1,
			category = '',
			size = '',
			brand = '',
			color = '',
			available = 0,
			sortby = 'name-inc',
		} = req.query;
		const price = req.query.price ? req.query.price.split(',') : [0, 500];
		const minPrice = Number(price[0]) || 0;
		const maxPrice = Number(price[1]) || 500;

		const findCondition = {};
		if (category) {
			findCondition.categories = category;
		}
		if (size) {
			findCondition.size = size;
		}
		if (brand) {
			findCondition.brand = brand;
		}
		if (color) {
			findCondition.color = color;
		}
		if (+available === 1) {
			findCondition.quantity = { $gte: 0 };
		} else if (+available === -1) {
			findCondition.quantity = 0;
		}
		findCondition.price = { $lte: maxPrice, $gte: minPrice };

		const sortbySplit = sortby.split('-')[0] || 'name';
		const filterCondition = { [sortbySplit]: sortby.includes('dec') ? -1 : 1 };
		const CURRENT_PAGE = Math.max(page - 1, 0);

		const totalProducts = await Product.find(findCondition).countDocuments();

		const products = await Product.find(findCondition)
			.sort(filterCondition)
			.skip(CURRENT_PAGE * ITEM_PER_PAGE)
			.limit(ITEM_PER_PAGE);

		res.json({ products, totalProducts });
	} catch (err) {
		console.log(err);
		return next(err);
	}
};

exports.getProductById = async (req, res, next) => {
	try {
		const { productId } = req.params;

		const product = await Product.findById(productId, '-sold');

		if (!product) {
			return next(new HttpError('No product found!', 404));
		}

		const moreFromProduct = await Product.find(
			{
				_id: { $ne: product._id },
				brand: product.brand,
			},
			{
				photos: { $slice: 1 },
			},
		).limit(4);

		const alsoLikeProduct = await Product.find(
			{
				_id: { $ne: product._id },
				categories: { $in: product.categories },
			},
			{ name: 1, photos: { $slice: 1 } },
		).limit(6);

		res.json({ product, moreFromProduct, alsoLikeProduct });
	} catch (err) {
		console.log(err);
		return next(err);
	}
};

exports.getSearchProductByName = async (req, res, next) => {
	try {
		const { name = '' } = req.query;

		const products = await Product.find(
			{
				name: { $regex: new RegExp(name, 'gi') },
			},
			{ name: 1, photos: { $slice: 1 } },
		).limit(8);

		res.json({ products });
	} catch (err) {
		console.log(err);
		next(err);
	}
};
