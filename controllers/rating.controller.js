const { validationResult } = require('express-validator');

const Rating = require('../models/rating');
const Product = require('../models/product');
const HttpError = require('../models/http-error');

exports.postNewRating = async (req, res, next) => {
  try {
    const result = validationResult(req).array();

    if (result.length) {
      return next(new HttpError(result[0].msg), 422);
    }

    const { userId } = req;
    const { productId } = req.params;

    const product = await Product.findById(productId, 'rating');

    if (!product) {
      return next(new HttpError("Can't found this product!", 404));
    }

    product.rating.totalRating += 1;
    product.rating.totalScore += req.body.rating;

    await product.save();

    const rating = new Rating({ ...req.body, userId, productId });

    await rating.save();

    res.status(201).json(rating);
  } catch (err) {
    console.log(err);
    return next(err);
  }
};

exports.getRatingByProductId = async (req, res, next) => {
  try {
    const { productId } = req.params;
    const { page = 1 } = req.query;
    const ITEM_PER_PAGE = 5;

    const rating = await Rating.find({ productId })
      .populate('userId', 'name')
      .skip((page - 1) * ITEM_PER_PAGE)
      .limit(ITEM_PER_PAGE)
      .sort({ createdAt: -1 });

    res.json({ rating });
  } catch (err) {
    console.log(err);
    return next(err);
  }
};

exports.deleteRatingById = async (req, res, next) => {
  const { userId } = req;
  const { ratingId, productId } = req.params;

  try {
    const rating = await Rating.findById(ratingId);

    if (rating.userId.toString() !== userId) {
      return next(new HttpError('Not authorization!', 403));
    }

    const product = await Product.findById(productId);

    product.rating.totalRating -= 1;
    product.rating.totalScore -= rating.rating;

    await product.save();

    await rating.remove();

    res.json({ message: 'Delete rating success' });
  } catch (err) {
    console.log(err);
    return next(err);
  }
};
