const mongoose = require('mongoose');

const Product = require('./models/product');

const randomImg = Array.from(Array(24), (v, i) => i + 1 + '.jpg');
const randomSize = ['s', 'm', 'l', 'xl', 'xxl'];
const randomBrands = ['zara', 'h-m', 'pull-bear', 'dior', 'chanel'];
const randomCategories = [
	'rompers-jumpsuits',
	'casual-dresses',
	'going-out-dresses',
	'party-ocassion-dresses',
	'mini-dresses',
	'maxi-midi-dresses',
	'sets',
];
const randomColor = ['green', 'blue', 'white', 'black', 'red'];

function suffleArrayAndSlice(array, length) {
	return array.sort(() => Math.random() - 0.5).slice(0, length);
}

function randomBoolean(correct, wrong) {
	return Math.random() >= 0.5 ? correct : wrong;
}

const addDb = async () => {
	for (let i = 1; i <= 120; i++) {
		const randomProduct = {
			name: `Áo thun ${randomBoolean('nam', 'nữ')} ${randomBoolean(
				'Cotton',
				'Jean',
			)} ${randomBoolean('Compact', 'Polo')} ${i}`,
			description: 'Người mẫu: 1m84 - 73kg * Mặc size 2XL',
			price: ~~(Math.random() * 450) + 50,
			quantity: ~~(Math.random() * 100),
			brand: suffleArrayAndSlice(randomBrands, 1)[0],
			categories: suffleArrayAndSlice(
				randomCategories,
				~~(Math.random() * 6) + 1,
			),
			color: suffleArrayAndSlice(randomColor, ~~(Math.random() * 3) + 3),
			size: suffleArrayAndSlice(randomSize, ~~(Math.random() * 3) + 3),
			photos: suffleArrayAndSlice(randomImg, ~~(Math.random() * 3) + 6),
		};

		const product = new Product(randomProduct);

		await product.save();
	}

	mongoose.connection.close();
};

mongoose
	.connect('', { useUnifiedTopology: true, useNewUrlParser: true })
	.then(addDb);
